// TODO use typesafe-actions
import { Action, Dispatch } from "redux";
import { Request, Response } from "rest";
import { wrappedRestClient as client } from "../../common/api/client";
import { addToEntityPage, entityPageLoaded, errorAction } from "../app/actions";
import { PageRequest } from "../app/reducers";
import { State } from "../State";
import {
  entityCreateRequest, entityDeleteRequest, entityFetchRequest, entityRelateRequest,
  entitySchemaRequest, entityUnrelateRequest, entityUpdateRequest,
  halProfileRequest, jsonSchemaRequest,
  relatedEntitiesRequest, root
} from "./requests";
import {
  CANCEL_EDIT_ENTITY_LOCAL,
  CREATE_ENTITY,
  DELETE_ENTITY,
  EDIT_ENTITY_LOCAL,
  Entity,
  ENTITY_CREATED,
  ENTITY_DELETED,
  ENTITY_LINKED, ENTITY_UNLINKED,
  ENTITY_UPDATED,
  EntitySchema,
  LINK_ENTITY,
  RECEIVE_ENTITIES,
  RECEIVE_ENTITY_PROFILES,
  REQUEST_ENTITIES,
  REQUEST_ENTITY_PROFILES,
  SchemaProperty,
  UNLINK_ENTITY,
  UPDATE_ENTITY
} from "./types";


/**
 * TODO.   This is horrible.  Replace it with something
 * the reducer can consume more cleanly.
 */
export interface EntityAction extends Action {
  entity?: Entity;
  entities?: Entity[];
  schema?: EntitySchema;
  errorMessage?: string;
  schemaByRel?: {
    [rel: string]: EntitySchema
  };
  schemaByUri?: {
    [uri: string]: EntitySchema
  };
  relatedEntity?: Entity;
  property?: SchemaProperty;
}


/* *********************************************************
 *** These are the methods around retrieving the   ********
 * entity schema and profile information ******************
 ******************************************************** */

/**
 * Retrieve the profiles of all the entities including the json schema and alps profile for each
 */
export const fetchEntityProfiles = (onSuccess: () => void) => async (dispatch: Dispatch) => {
  dispatch({ "type": REQUEST_ENTITY_PROFILES });
  const jsonSchemaResponse = await clientRequest(jsonSchemaRequest(), dispatch, "error_retreiving_json_schema");

  if (jsonSchemaResponse) {
    if (typeof(jsonSchemaResponse.entity) === "string" || jsonSchemaResponse.entity instanceof String) {
      errorAction("Error initializing: ", jsonSchemaResponse.entity + "")(dispatch);
    } else {
      // First the hrefs need to be dereference-able because sometimes things are
      // reference by rel, and sometimes by href
      const hrefToRelMap = jsonSchemaResponse.entity.links.reduce((map: { [key: string]: string }, link: { rel: string, href: string }) => {
        const { rel, href } = link;
        map[href] = rel;
        return map;
      }, {});


      const schemaByUri = {};
      const schemaByRel = {};

  // Fetch the profile definition for each type of entity
      for (const entityLink of jsonSchemaResponse.entity.links) {
        if (entityLink.rel !== "self") {
          const entitySchema = await buildEntitySchema(entityLink.href, dispatch, hrefToRelMap);
          schemaByUri[entityLink.href] = entitySchema;
          schemaByRel[entityLink.rel] = entitySchema;
        }
      }
      const action: EntityAction = {
        "schemaByRel": schemaByRel,
        "schemaByUri": schemaByUri,
        "type": RECEIVE_ENTITY_PROFILES
      };
      onSuccess();
      dispatch(action);
    }
  }
};

// Once the schema and profile are built, a schema for each entity type can be created.
const buildEntitySchema = async (href: string, dispatch: Dispatch, hrefToRelMap: {[href: string]: string}) => {
// The entities are described both by "what they are" and "what they can do"
  // HAL schema is what they are
  const linkRequest = entitySchemaRequest(href);
  const linkResponse = await clientRequest(linkRequest, dispatch, "error_retrieving_entity_schema");
  // The entities are described both by "what they are" and "what they can do"
  // HAL is what they can do
  const halRequest = halProfileRequest(href);
  const halProfileResponse = await clientRequest(halRequest, dispatch, "error_retrieving_hal_profile");
  if (halProfileResponse) {
    const alpsPropertyDescriptor = extractAlpsDescriptors(halProfileResponse, href);
    const propertyUriMap = mapRelationshipPropertiesByUri(alpsPropertyDescriptor);
    if (linkResponse) {
      const schemaProperties = extractSchemaProperties(linkResponse, propertyUriMap, hrefToRelMap);
      const rel = hrefToRelMap[href];
      const entitySchema: EntitySchema = {
        "endpoint": `${root}/${rel}`,
        "profile": halProfileResponse.entity.alps.descriptors,
        "properties": schemaProperties,
        "rel": rel,
        "title": linkResponse.entity.title,
        "uri": href
      };
      return entitySchema;
    } else {
      return null;
    }
  } else {
    return null;
  }
};

// Retrieve the properties from the json schema
const extractSchemaProperties = (linkResponse: Response, propertyUriMap: {[uri: string]: string}, hrefToRelMap: { [href: string]: string }) => {
// Map all the properties keyed either by name or their uri.
  const schemaProperties = Object.keys(linkResponse.entity.properties).reduce((map: { [key: string]: SchemaProperty }, name: string) => {
    const property = linkResponse.entity.properties[name];
    map[name] = property;
    map[name].name = name;
    const hashReturnTypeUri = propertyUriMap[name];
    if (hashReturnTypeUri) {
      const returnTypeUri = hashReturnTypeUri.substr(0, hashReturnTypeUri.indexOf("#"));
      map[name].returnTypeUri = returnTypeUri;
      map[name].returnTypeRel = hrefToRelMap[returnTypeUri];
    }
    return map;
  }, {} as { [key: string]: SchemaProperty });
  return schemaProperties;
};

// Retrieve the relationship properties from the alps descriptor.
const mapRelationshipPropertiesByUri = (alpsPropertyDescriptor: { descriptor: Array<{ name: string; rt?: string }> }) => {
// Map names to href if it exists.  The href should be used to key on related entities
  const propertyUriMap = alpsPropertyDescriptor.descriptor.reduce((map: { [key: string]: string }, descriptor: { name: string; rt?: string }) => {
    if (descriptor.rt) {
      map[descriptor.name] = descriptor.rt;
    }
    return map;
  }, {} as { [key: string]: string });
  return propertyUriMap;
};

// Extract the alps descriptor from the alps profile
const extractAlpsDescriptors = (halProfileResponse: Response, href: string) => {
// The alps descriptor has to be dug out based on href equality to dereference the uris to the property types that have them
  let alpsPropertyDescriptor;
  for (const descriptor of halProfileResponse.entity.alps.descriptor) {
    if (descriptor.href && descriptor.href === href) {
      alpsPropertyDescriptor = descriptor;
      break;
    }
  }
  return alpsPropertyDescriptor;
};


/* *********************************************************
 *** These are the methods around entity CRUD   ************
 ******************************************************** */
/**
 * Thunk that receives a dispatch.  It creates an dispatching the corresponding
 * request and receive actions where appropriate.
 */
export const createEntity = (nascentEntity: Entity, pageCollectionName: string) => async (dispatch: Dispatch) => {
  try {
    dispatch(createEntityRequest(nascentEntity.schema));
    const createRequest = entityCreateRequest(nascentEntity);
    const entityResponse = await clientRequest(createRequest, dispatch, "error_creating_entity");
    if (entityResponse) {
      const newEntity = entityResponse.entity;
      dispatch(entityCreated(decorateEntities([newEntity], nascentEntity.schema, dispatch)[0]));
      dispatch(addToEntityPage(newEntity, pageCollectionName));
    }
  } catch (error) {
    errorAction("error_creating_entity", error.message)(dispatch);
  }
};
// TODO respond to websockets
/**
 * Thunk that receives a dispatch.  It fetches entities dispatching the corresponding
 * request and receive actions where appropriate.
 */
export const requestEntities = (entitySchema: EntitySchema,
                                pageRequest: PageRequest,
                                fetchRelated: boolean = true) => async (dispatch: Dispatch<EntityAction>, getState: () => State) => {
  try {
    dispatch(requestEntitiesRequested(entitySchema));
    const fetchRequest = entityFetchRequest(entitySchema, pageRequest);
    const entityResponse = await clientRequest(fetchRequest, dispatch, "error_requesting_entities");
    if (entityResponse) {
      const entityObj = entityResponse.entity;
      const entityItems = entityObj._embedded[entitySchema.rel];
      const schemaByUri = getState().entityStore.schemaByUri;
      const entities = decorateEntities(entityItems, entitySchema, dispatch, schemaByUri, fetchRelated);
      dispatch(receiveEntities(entities, entitySchema));
      dispatch(entityPageLoaded(entities, pageRequest.collectionName));
    }
  } catch (error) {
    errorAction("error_requesting_entity", error.message)(dispatch);
  }
};
/**
 * Thunk that receives a dispatch.  It fetches entities dispatching the corresponding
 * request and receive actions where appropriate.
 */
export const updateEntity = (entity: Entity) => async (dispatch: Dispatch<EntityAction>) => {
  try {
    dispatch(updateEntityRequested(entity.schema));
    const updateRequest = entityUpdateRequest(entity);
    const entityResponse = await clientRequest(updateRequest, dispatch, "error_updating_entities");
    if(entityResponse) {
      const updatedEntity = entityResponse.entity;
      dispatch(entityUpdated(decorateEntities([updatedEntity], entity.schema, dispatch)[0]));
    }
  } catch (error) {
    errorAction("error_updating_entities", error.message)(dispatch);
  }
};

/**
 * Thunk that receives a dispatch.  It fetches entities dispatching the corresponding
 * request and receive actions where appropriate.
 */
export const deleteEntity = (entity: Entity) => async (dispatch: Dispatch<EntityAction>) => {
  try {
    dispatch(deleteEntityRequested(entity.schema));
    const deleteRequest = entityDeleteRequest(entity);
    await clientRequest(deleteRequest, dispatch, "error_deleting_entity");
    dispatch(entityDeleted(entity));
  } catch (error) {
    errorAction("error_deleting_entity", error.message)(dispatch);
  }
};

export const relateEntity = (owningEntity: Entity, relatedEntity: Entity, property: SchemaProperty) => async (dispatch: Dispatch<EntityAction>) => {
  dispatch(linkEntity(owningEntity, relatedEntity, property));
  const relateRequest = entityRelateRequest(owningEntity, property, relatedEntity);
  await clientRequest(relateRequest, dispatch, "error_relating_entities");
  dispatch(entityLinked(owningEntity, relatedEntity, property));
};

export const unrelateEntity = (owningEntity: Entity, noLongerRelated: Entity, property: SchemaProperty) => async (dispatch: Dispatch<EntityAction>) => {
  dispatch(unlinkEntity(owningEntity, noLongerRelated, property));
  const unrelateRequest = entityUnrelateRequest(owningEntity, property, noLongerRelated);
  await clientRequest(unrelateRequest, dispatch, "error_unrelating_entities");
  dispatch(entityUnlinked(owningEntity, noLongerRelated, property));
};

// TODO this should definitely be deferred until its needed
const getRelatedEntities = async (entity: Entity, property: SchemaProperty, relatedSchema: EntitySchema, dispatch: Dispatch) => {
  if (entity._links) {
    const propertyName = property.name;
    const relationshipUri = entity._links[propertyName].href;
    let relatedObject;
    try {
      const getRelatedEntityRequest = relatedEntitiesRequest(relationshipUri);
      relatedObject = await clientRequest(getRelatedEntityRequest, dispatch, "error_getting_related_entities", 405);
    } catch (e) {
      // Sometimes related objects just don't exist.  There are none.
    }
    let entities;
    if(relatedObject) {
      let singleValued = false;
      if (!relatedObject.entity._embedded) {
        singleValued = true;
      }
      if (singleValued) {
        if (relatedObject.entity) {
          entities = decorateEntities([relatedObject.entity], relatedSchema, dispatch);// TODO remove the unneeded _links from the data
          const entityUri = entities.length === 1 ? entities[0].uri : null;
          entity[propertyName] = entityUri;
        }
      } else {
        const embedded = relatedObject.entity._embedded;
        // There is no HATEOAS description of what that key is. So its looked up by property rel
        const entityData = embedded[property.returnTypeRel];
        entities = decorateEntities(entityData, relatedSchema, dispatch);
        const entityUris = entities.map((entityElement: Entity) => entityElement.uri);
        entity[propertyName] = entityUris;
      }
    }
    if (entities) {
      dispatch(receiveEntities(entities, relatedSchema));
    }
  }
};

/**
 * Massages entity data into first class entities
 */
const decorateEntities = (entityItems: Entity[], entitySchema: EntitySchema, dispatch: Dispatch, schemaByUri?: {[key: string]: EntitySchema}, fetchRelated: boolean = true) => {
  const entities: Entity[] = entityItems.map((entity: Entity) => {
    entity.schema = entitySchema;
    if (entity._links) {
      for (const propertyName in entitySchema.properties) {
        if (entity._links.hasOwnProperty(propertyName)) {
          const property = entitySchema.properties[propertyName];
          if (property.format === "uri") {
            // TODO this is a lot of database work.  Maybe you should defer a bunch of it.
            if (schemaByUri && fetchRelated) {
              getRelatedEntities(entity, entitySchema.properties[propertyName], schemaByUri[property.returnTypeUri], dispatch);
            }
          }
        }
      }
      entity.uri = entity._links ? entity._links.self.href : "";
    }
    entity.id = entity.uri.substr(entity.uri.lastIndexOf(("/")) + 1);
    return entity;
  });
  return entities;
};


/**
 * Action indicating an entity create request response has been sent
 */
export const createEntityRequest = (entitySchema: EntitySchema): EntityAction => ({
  "schema": entitySchema,
  "type": CREATE_ENTITY
});

/**
 * Action indicating an entity create request response has been received
 */
export const entityCreated = (entity: Entity): EntityAction => ({
  "entity": entity,
  "type": ENTITY_CREATED
});

/**
 * Action indicating entity request is being made
 */
export const requestEntitiesRequested = (entitySchema: EntitySchema): EntityAction => ({
  "schema": entitySchema,
  "type": REQUEST_ENTITIES
});


/**
 * Action indicating entities have been received
 */
export const receiveEntities = (entities: Entity[], entitySchema: EntitySchema): EntityAction => ({
  "entities": entities,
  "schema": entitySchema,
  "type": RECEIVE_ENTITIES
});

/**
 * Action indicating entity update request is being made
 */
export const updateEntityRequested = (entitySchema: EntitySchema): EntityAction => ({
  "schema": entitySchema,
  "type": UPDATE_ENTITY
});

/**
 * Action indicating an entity update request has been received
 */
export const entityUpdated = (entity: Entity): EntityAction => ({
  "entity": entity,
  "type": ENTITY_UPDATED
});

/**
 * Action indicating entity delete request is being made
 */
export const deleteEntityRequested = (entitySchema: EntitySchema): EntityAction => ({
  "schema": entitySchema,
  "type": DELETE_ENTITY
});

/**
 * Action indicating an entity delete request has been received
 */
export const entityDeleted = (entity: Entity) => ({
  "entity": entity,
  "type": ENTITY_DELETED
});

/**
 * Action indicating that entity data is actively being modified
 */
export const editEntityLocal = (entity: Entity): EntityAction => ({
  "entity": entity,
  "type": EDIT_ENTITY_LOCAL
});

/**
 * Action indicating that modelVariant data is modification should end
 */
export const cancelEditEntityLocal = (): EntityAction => ({
  "type": CANCEL_EDIT_ENTITY_LOCAL
});

/**
 */
export const linkEntity = (entity: Entity, relatedEntity: Entity, property: SchemaProperty): EntityAction => ({
  "entity": entity,
  "property": property,
  "relatedEntity": relatedEntity,
  "type": LINK_ENTITY,
});

/**
 */
export const entityLinked = (entity: Entity, relatedEntity: Entity, property: SchemaProperty): EntityAction => ({
  "entity": entity,
  "property": property,
  "relatedEntity": relatedEntity,
  "type": ENTITY_LINKED
});

/**
 */
export const unlinkEntity = (entity: Entity, relatedEntity: Entity, property: SchemaProperty): EntityAction => ({
  "entity": entity,
  "property": property,
  "relatedEntity": relatedEntity,
  "type": UNLINK_ENTITY,
});

/**
 */
export const entityUnlinked = (entity: Entity, relatedEntity: Entity, property: SchemaProperty): EntityAction => ({
  "entity": entity,
  "property": property,
  "relatedEntity": relatedEntity,
  "type": ENTITY_UNLINKED
});

const clientRequest = async (request: Request, dispatch: Dispatch, errorMessageKey: string, maximumError: number = 299) => {
  const response = await client(request);
  if (!errorDispatched(response, dispatch, errorMessageKey, maximumError)) {
    return response;
  } else {
    return null;
  }
};

/**
 * Dispatches an error if the response status is an error status
 * otherwise return false
 */
const errorDispatched = (response: Response, dispatch: Dispatch<EntityAction>, errorMessageKey: string, maximumErrorAllowed: number) => {
  if (response.status.code > maximumErrorAllowed) {
    errorAction(errorMessageKey, `Url: ${response.request.path}\n ${response.status.code} ${response.status.text}`)(dispatch);
    return true;
  } else {
    return false;
  }
};
