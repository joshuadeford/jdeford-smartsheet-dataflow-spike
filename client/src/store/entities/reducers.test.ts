import { entityStore, EntityStore } from "./reducers";

import {
  CREATE_ENTITY, DELETE_ENTITY,
  ENTITY_CREATED, ENTITY_DELETED,
  ENTITY_UPDATED, EntitySchema, RECEIVE_ENTITIES,
  REQUEST_ENTITIES, UPDATE_ENTITY
} from "./types";

it("should return the initial state", () => {
  expect(entityStore(undefined, { "type": "None" })).toEqual({
    "isFetching": false,
    "items": []
  });
});
// TODO make a test that ensures state is NOT being modified
it("should handle CREATE_ENTITY", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": {}, "entityProfiles": {}, "schemaByUri": {}, "schemaByRel": {} } as EntityStore, {
        "type": CREATE_ENTITY
      })
  ).toEqual({
      "isFetching": true,
      "items": []
   });
});

it("should handle ENTITY_CREATED", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": {  }, "schemaByUri": {}  } as EntityStore, {
        "entity": { "entityName": "", "id": "1", "uri": "Fred", "schema": {} as EntitySchema },
        "type": ENTITY_CREATED
      })
  ).toEqual({
      "isFetching": false,
      "items": [{ "id": 1, "entityName": "Fred" }]
   });
});

it("should handle REQUEST_ENTITIES", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": {}, "schemaByUri": {}, "schemaByRel": {}  }, {
        "type": REQUEST_ENTITIES
      })
  ).toEqual({
      "isFetching": true,
      "items": []
   });
});


it("should handle RECEIVE_ENTITIES", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": {}, "schemaByUri": {}, "schemaByRel": {} }, {
        "entities": [],
        "type": RECEIVE_ENTITIES
      })
  ).toEqual({
    "isFetching": false,
    "items": [{ "id": 1, "entityName": "Fred" }]
  });
});


it("should handle UPDATE_ENTITY", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": { }, "schemaByUri": {}, "schemaByRel": {} }, {
        "type": UPDATE_ENTITY
      })
  ).toEqual({
      "isFetching": true,
      "items": [{ "id": "1", "entityName": "Fred" }]
   });
});


it("should handle ENTITY_UPDATED", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": { }, "schemaByUri": {}, "schemaByRel": {} }, {
        "entity": { "entityName": "", "id": "1", "uri": "Freddie", "schema": {} as EntitySchema },
        "type": ENTITY_UPDATED
      })
  ).toEqual({
      "isFetching": false,
      "items": [{ "id": "1", "entityName": "Freddie" }]
   });
});


it("should handle DELETE_ENTITY", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": { }, "schemaByUri": {}, "schemaByRel": {} }, {
        "type": DELETE_ENTITY
      })
  ).toEqual({
      "isFetching": true,
      "items": [{ "id": "1", "entityName": "Fred" }]
   });
});


it("should handle ENTITY_DELETED", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": { }, "schemaByUri": {}, "schemaByRel": {} }, {
        "entity": { "entityName": "", "id": "1", "uri": "Fred", "schema": { } as EntitySchema },
        "type": ENTITY_DELETED
      })
  ).toEqual({
      "isFetching": false,
      "items": []
   });
});
