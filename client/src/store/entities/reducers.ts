import { EntityAction } from "./actions";
import {
  CANCEL_EDIT_ENTITY_LOCAL,
  CREATE_ENTITY,
  DELETE_ENTITY,
  EDIT_ENTITY_LOCAL, Entity,
  ENTITY_CREATED,
  ENTITY_DELETED, ENTITY_LINKED, ENTITY_UNLINKED,
  ENTITY_UPDATED, EntitySchema, LINK_ENTITY,
  RECEIVE_ENTITIES,
  RECEIVE_ENTITY_PROFILES,
  REQUEST_ENTITIES,
  REQUEST_ENTITY_PROFILES, SchemaProperty, UNLINK_ENTITY,
  UPDATE_ENTITY
} from "./types";

/**
 * The itemMapsByRel object looks something like this
 * items = {
 *   users: {
 *     http://server/api/users/F894AB: {
 *       first: john,
 *       last: smith,
 *       salary: 100000000000000,
 *       devices: [
 *         http://server/api/device/FF453E,//URI to the device
 *         http://server/api/device/A4B24C,
 *       ]
 *     }
 *   },
 *   devices...
 * }
 */
export interface EntityStore {
  isFetching: boolean;
  schemaByUri: { [uri: string]: EntitySchema};
  schemaByRel: { [rel: string]: EntitySchema};
  itemMapsByRel: {
    [entityRel: string]: {
      [entityUri: string]: Entity
    }
  };
  editingItem?: Entity;// The item currently being edited.  Only one thing can be edited at a time.
}

const initialState = {
  "isFetching": false,
  "itemMapsByRel": { },
  "schemaByRel": { },
  "schemaByUri": { },
};

// TODO entity metadata should probably be scoped into a 'special' property so
// none of the properties clash with entity properties like uri and properties
// TODO the component the reducers export should be called stores  entityStore: EntityStore
export const entityStore = (state: EntityStore = initialState, action: EntityAction) => {
  switch (action.type) {
    case REQUEST_ENTITY_PROFILES:
      return Object.freeze({
        ...state,
        "isFetching": true
      });
    case RECEIVE_ENTITY_PROFILES:
      const schemaByRel = action.schemaByRel as EntityStore["schemaByRel"];
      const schemaByUri = action.schemaByUri as EntityStore["schemaByUri"];
      // Initialize all the items
      const itemsByRel = Object.keys(schemaByRel).reduce((itemMap: EntityStore["itemMapsByRel"], reln: string) => {
        itemMap[reln] = {};
        return itemMap;
      }, {});
      return Object.freeze({
        ...state,
        "itemMapsByRel": itemsByRel,
        "schemaByRel": schemaByRel,
        "schemaByUri": schemaByUri,
      });
    case LINK_ENTITY:
    case UNLINK_ENTITY:
    case DELETE_ENTITY:
    case UPDATE_ENTITY:
    case CREATE_ENTITY:
    case REQUEST_ENTITIES:
      return Object.freeze({
        ...state,
        "isFetching": true
      });
    case ENTITY_UPDATED:
    case ENTITY_CREATED:
        return Object.freeze(addEntityToState(state, action.entity as Entity));
    case RECEIVE_ENTITIES:
      // TODO some sort of purging mechanism so all the entities in the system don't eventually get loaded.
      // Browsing media assets could blow out memory for example
      const newEntities = action.entities as Entity[];
      const schema = action.schema as EntitySchema;
      const rel = schema.rel;

     const newEntitiesByUri = newEntities.reduce((map: {[uri: string]: Entity}, entityItem: Entity) => {
        map[entityItem.uri] = entityItem;
        return map;
      }, {});
      return Object.freeze({
        ...state,
        "isFetching": false,
        "itemMapsByRel": {
          ...state.itemMapsByRel,
          [rel]: { ...state.itemMapsByRel[rel], ...newEntitiesByUri }
        }
      });
    case CANCEL_EDIT_ENTITY_LOCAL:
      return {
        ...state,
        "editingItem": undefined,
        "isFetching": false
      };
    case EDIT_ENTITY_LOCAL:
      return Object.freeze({
        ...state,
        "editingItem": { ...action.entity },
        "isFetching": false
      });
    case ENTITY_DELETED:
      const deletedEntity = (action.entity as Entity);
      const entitiesByUriDup = { ...state.itemMapsByRel[deletedEntity.schema.rel] };
      delete entitiesByUriDup[deletedEntity.uri];

      return Object.freeze({
        ...state,
        "isFetching": false,
        "itemMapsByRel": {
          ...state.itemMapsByRel,
          [deletedEntity.schema.rel]: {
            ...entitiesByUriDup
          }
        }
      });
    case ENTITY_LINKED:
      const owningEntity = { ...(action.entity as Entity) };
      const relatedEntity = (action.relatedEntity as Entity);
      const property = (action.property as SchemaProperty);
      let propertyValue = owningEntity[property.name];
      if (typeof propertyValue !== "undefined" && propertyValue instanceof Array) {
        propertyValue = propertyValue.slice();// Make a copy to keep from modifying state
        propertyValue.push(relatedEntity.uri);
        owningEntity[property.name] = propertyValue;
      } else {// Single valued properties are either undefined or not an array
        owningEntity[property.name] = relatedEntity.uri;
      }
      // TODO the other side of the relationship is not being added here if it even exists.

      return Object.freeze(addEntityToState(state, owningEntity));
    case ENTITY_UNLINKED:
      const owningEntityCopy = { ...(action.entity as Entity) };
      const related = (action.relatedEntity as Entity);
      const prop = (action.property as SchemaProperty);
      const propValue = owningEntityCopy[prop.name];
      if (propValue instanceof Array) {
        const uriIndex = propValue.indexOf(related.uri);
        // Make a copy then splice it.
        owningEntityCopy[prop.name] = propValue.slice();
        owningEntityCopy[prop.name].splice(uriIndex, uriIndex + 1);
      } else {
        delete owningEntityCopy[prop.name];
      }
      // TODO the other side of the relationship is not being removed here if it even exists.

      return Object.freeze(addEntityToState(state, owningEntityCopy));
    default:
      return state;
  }

};

/**
 * Makes a copy of the state with the specified entity (not a copy of it) added to it.
 */
const addEntityToState = (state: EntityStore, entityCopy: Entity) => {
  const entityUri = entityCopy.uri;
  const entityRel = entityCopy.schema.rel;
  return {
    ...state,
    "editingItem": undefined,
    "isFetching": false,
    "itemMapsByRel": {
      ...state.itemMapsByRel,
      [entityRel]: {
        ...state.itemMapsByRel[entityRel],
        [entityUri]: entityCopy
      }
    }
  };
};

