import * as fetchMock from "fetch-mock";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { createEntity, deleteEntity, requestEntities, updateEntity } from "./actions";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

import { PageRequest } from "../app/reducers";
import {
  CREATE_ENTITY, DELETE_ENTITY,
  ENTITY_CREATED, ENTITY_DELETED, ENTITY_UPDATED,
  EntitySchema, RECEIVE_ENTITIES,
  REQUEST_ENTITIES, UPDATE_ENTITY
} from "./types";

afterEach(() => {
  fetchMock.reset();
  fetchMock.restore();
});

// TODO test onSuccess and onError for error conditions
it("creates CREATE_ENTITY and ENTITY_CREATED when creating an entity has been done", () => {
  fetchMock.postOnce("/api/entities", {
    "body": [{ "typeName": "Mork FromOrk" }],
    "headers": { "content-type": "application/json" }
  });

  const expectedActions = [
    { "type": CREATE_ENTITY },
    {
      "createdEntity": [{ "typeName": "Mork FromOrk" }],
      "placeholder": { "typeName": "Mork FromOrk" },
      "type": ENTITY_CREATED
    }
  ];

  const store = mockStore({ "entities": [] });

  return store.dispatch(createEntity({ "id": "None", "schema": ({} as EntitySchema), "uri": "uuu", "entityName": "som"  }, "colle") as any).then(() => {
    // return of async actions
    expect(store.getActions()).toEqual(expectedActions);
  });
});


it("creates REQUEST_ENTITIES and RECEIVE_ENTITIES when fetching entities has been done", () => {
  fetchMock.getOnce("/api/entities", {
    "body": [{ "typeName": "Mork FromOrk" }],
    "headers": { "content-type": "application/json" }
  });

  const expectedActions = [
    { "type": REQUEST_ENTITIES },
    { "type": RECEIVE_ENTITIES, "entities": [{ "typeName": "Mork FromOrk" }] }
  ];

  const store = mockStore({ "entities": [] });

  return store.dispatch(requestEntities({
    "endpoint": "localhost",
    "profile": {},
    "properties": {},
    "rel": "",
    "title": "entityTitle",
    "uri": "Entity"
  }, {} as PageRequest) as any).then(() => {

    // return of async actions
    expect(store.getActions()).toEqual(expectedActions);
  });
});

it("creates UPDATE_ENTITY and ENTITY_UPDATED when update and entity has been done", () => {
  fetchMock.patchOnce("/entities/1", {
    "body": [{ "typeName": "Mork FromOrk" }],
    "headers": { "content-type": "application/json" }
  });

  const expectedActions = [
    { "type": UPDATE_ENTITY },
    { "type": ENTITY_UPDATED, "entity": [{ "typeName": "Mork FromOrk" }] }
  ];

  const store = mockStore({ "entities": [] });

  return store.dispatch(updateEntity({ "entityName": "", "uri": "Mork FromOrkee", "id": "1", "schema": ( {} as EntitySchema) }) as any).then(({ /* */ }) => {
    // return of async actions
    expect(store.getActions()).toEqual(expectedActions);
  });
});

it("creates DELETE_ENTITY and ENTITY_DELETED when deleting and entity has been done", () => {
  fetchMock.deleteOnce("/entities/1", {
    "body": [{ "typeName": "Mork FromOrkee" }],
    "headers": { "content-type": "application/json" }
  });

  const expectedActions = [
    { "type": DELETE_ENTITY },
    { "entity": { "typeName": "Mork FromOrkee", "id": 1 }, "type": ENTITY_DELETED }
  ];


  const store = mockStore({ "entities": [] });

  // TODO get rid of onError and onSuccess
  return store.dispatch(deleteEntity({ "entityName": "", "uri": "Mork FromOrkee", "id": "1", "schema": ({ } as EntitySchema) },
  ) as any).then(() => {
    // return of async actions
    expect(store.getActions()).toEqual(expectedActions);
  });
});
