import { Request } from "rest";
import { PageRequest } from "../app/reducers";
import { Entity, EntitySchema, SchemaProperty } from "./types";

export const root = "/api";

export const entitySchemaRequest = (path: string): Request => {
  return {
    "headers": { "Accept": "application/schema+json" },
    "method": "GET",
    "path": path
  };
};

export const jsonSchemaRequest = (): Request => {
  // Fetch the list of all the entity types and their corresponding links to their type
  // definition.
  return {
    "headers": { "Accept": "application/schema+json" },
    "method": "GET",
    // This SHOULD be the only url you will find.  HAL/ALPS are formats that describe the REST api this stuff is consuming
    "path": `${root}/profile`
  };
};

export const halProfileRequest = (path: string): Request => {
  return {
    "headers": { "Accept": "application/hal+json" },
    "method": "GET",
    "path": path
  };
};

export const entityCreateRequest = (nascentEntity: Entity): Request => {
  return {
    "entity": nascentEntity,
    "headers": { "Content-Type": "application/json" },
    "method": "POST",
    "path": nascentEntity.schema.endpoint,
  };
};

export const entityRelateRequest = (owningEntity: Entity, property: SchemaProperty, relatedEntity: Entity): Request => {
  // TODO these should navigate the HAL api
  return {
    "entity": relatedEntity,
    "headers": {
      "Accept": "*/*",
      "Content-Type": "text/uri-list"
    },
    "method": "PUT",
    "path": `${owningEntity.uri}/${property.name}`,
    // "body": relatedEntity.uri// If more than one uri is going to be provided they are '/n' delimited
  };
};

export const entityFetchRequest = (entitySchema: EntitySchema, pageRequest: PageRequest): Request => {
  return {
    "method": "GET",
    "path": `${entitySchema.endpoint}?page=${pageRequest.page}&size=${pageRequest.size}&sort=${pageRequest.sort}`
  };
};

export const relatedEntitiesRequest = (relationshipUri: string): Request => {
  const getRelatedEntityRequest = {
    "method": "GET",
    "path": relationshipUri
  };
  return getRelatedEntityRequest;
};

export const entityUpdateRequest = (entity: Entity): Request => {
  return {
    "entity": entity,
    "headers": { "Content-Type": "application/json" },
    "method": "PATCH",
    "path": entity.uri,
  };
};

export const entityDeleteRequest = (entity: Entity): Request => {
  return {
    "method": "DELETE",
    "path": entity.uri
  };
};

export const entityUnrelateRequest = (owningEntity: Entity, property: SchemaProperty, noLongerRelated: Entity): Request => {
// TODO these should navigate the HAL api
  return {
    "method": "DELETE",
    "path": `${owningEntity.uri}/${property.name}/${noLongerRelated.id}`
  };
};
