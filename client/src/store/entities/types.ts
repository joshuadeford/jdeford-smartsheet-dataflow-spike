const namespace = "ONPREM_OKS_ENTITIES";

export const ERROR = `${namespace}@CREATE_ERROR`;

export const REQUEST_ENTITY_PROFILES = `${namespace}@REQUEST_ENTITY_PROFILES`;
export const RECEIVE_ENTITY_PROFILES = `${namespace}@RECEIVE_ENTITY_PROFILES`;

export const CREATE_ENTITY = `${namespace}@CREATE_ENTITY`;
export const ENTITY_CREATED = `${namespace}@ENTITY_CREATED`;

export const REQUEST_ENTITIES = `${namespace}@REQUEST_ENTITIES`;
export const RECEIVE_ENTITIES = `${namespace}@RECEIVE_ENTITIES`;

export const UPDATE_ENTITY = `${namespace}@UPDATE_ENTITY`;
export const ENTITY_UPDATED = `${namespace}@EMPOLOYEE_UPDATED`;

export const UNLINK_ENTITY = `${namespace}@UNLINK_ENTITY`;
export const ENTITY_UNLINKED = `${namespace}@ENTITY_UNLINKED`;

export const LINK_ENTITY = `${namespace}@LINK_ENTITY`;
export const ENTITY_LINKED = `${namespace}@ENTITY_LINKED`;

export const DELETE_ENTITY = `${namespace}@DELETE_ENTITY`;
export const ENTITY_DELETED = `${namespace}@ENTITY_DELETED`;

export const EDIT_ENTITY_LOCAL = `${namespace}@EDIT_ENTITY_LOCAL`;
export const CANCEL_EDIT_ENTITY_LOCAL = `${namespace}@CANCEL_EDIT_ENTITY_LOCAL`;

export interface User extends Entity {
  userName?: string;
  DOB?: string;
  role?: string;
  editId?: string;
}

export interface Entity {
  uri: string;
  id: string;
  entityName: string;
  _links?: {
    self: {
      href: string
    }
  };
  schema: EntitySchema;
}

export interface EntitySchema {
  uri: string;
  title : string;
  endpoint : string;
  rel: string;// What an annoying name.  But this is how ALPS presents it so we'll keep it for now to prevent confusion.  This is basically the name of the endpoint and the object name returned from the endpoint.
  profile: any;// TODO consume and utilize the HAL metadata appropriately
  properties : {
    [propertyUri: string]: SchemaProperty
  };
}

export interface SchemaProperty {
  returnTypeUri: string;
  returnTypeRel: string;
  name: string;
  title: string;
  readOnly: boolean;
  type: string;
  format?: string;
}
