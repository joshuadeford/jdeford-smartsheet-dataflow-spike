import { RouterState } from "connected-react-router";
import { AppStore } from "./app/reducers";
import { EntityStore } from "./entities/reducers";

export interface State {
  router: RouterState;
  entityStore: EntityStore;
  appStore: AppStore;
}
