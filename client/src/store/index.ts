import { combineReducers } from "redux";
import { appStore } from "./app/reducers";

const rootReducer = combineReducers({
  "appStore": appStore,
});

export default rootReducer;
