import { AnyAction, Dispatch } from "redux";
import { Entity } from "../entities/types";
import {
  ADD_TO_PAGE,
  CLOSE_NAV_DRAWER,
  ERROR,
  ERROR_ACKNOWLEDGED,
  OPEN_NAV_DRAWER,
  PAGE_LOADED
} from "./types";

export const errorAcknowledged = () => (dispatch: Dispatch) => {
  dispatch({
    "type": ERROR_ACKNOWLEDGED
  });
};

export const errorAction = (messageKey: string, details: string) => (dispatch: Dispatch) => {
  dispatch({
    "details": details,
    "messageKey": messageKey,
    "type": ERROR
  });
};

/**
 * Action indicating the nav drawer should open
 */
export const openNavDrawer = () => ({
  "type": OPEN_NAV_DRAWER
});

/**
 * Action indicating the nav drawer should close
 */
export const closeNavDrawer = () => ({
  "type": CLOSE_NAV_DRAWER
});

export const addToEntityPage = (entity: Entity, pageCollectionName: string): AnyAction => {
  return {
    "collectionName": pageCollectionName,
    "element": entity.uri,
    "type": ADD_TO_PAGE,
  };
};

export const entityPageLoaded = (entities: Entity[], pageCollectionName: string): AnyAction => {
  const uris = entities.map((entity) => entity.uri);
  return {
    "collectionName": pageCollectionName,
    "elements": uris,
    "type": PAGE_LOADED
  };
};
