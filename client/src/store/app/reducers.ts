import { AnyAction } from "../../../node_modules/redux";
import {
  ADD_TO_PAGE,
  CLOSE_NAV_DRAWER, ERROR,
  ERROR_ACKNOWLEDGED,
  OPEN_NAV_DRAWER,
  PAGE_LOADED
} from "./types";

export interface AppStore {
  navDrawerOpen: boolean;
  pages: {// The name of the collection of pages so different components can use pages
    [pageCollectionName: string]: string[]
  };
  messageKey?: string;
  details?: string;
}

export interface PageRequest {
    collectionName: string;// The name of the collection of pages so different components can use pages
    page: number;// zero based page number
    size: number;// number of elements in the page
    sort: string;// a sort descriptor such as name:asc,salary:desc
}

const initialState = {
  "navDrawerOpen": false,
  "pages": {}
};

export const appStore = (state: AppStore = initialState, action: AnyAction) => {
  switch (action.type) {
    case ERROR:
      return Object.freeze({
        ...state,
        "details": action.details,
        "messageKey": action.messageKey
      });
    case ERROR_ACKNOWLEDGED:
      return Object.freeze({
        ...state,
        "details": undefined,
        "messageKey": undefined
      });
    case ADD_TO_PAGE:
      const collectionName = action.collectionName;
      const updatedPage = state.pages[collectionName].slice();
      updatedPage.unshift(action.element);
      return Object.freeze({
        ...state,
        "pages": {
          ...state.pages,
          [collectionName]: updatedPage
        }
      });
    case PAGE_LOADED:
      return Object.freeze({
        ...state,
        "pages": { ...state.pages, [action.collectionName]: action.elements }
      });
    case OPEN_NAV_DRAWER:
      return Object.freeze({
        ...state,
        "navDrawerOpen": true
      });
    case CLOSE_NAV_DRAWER:
      return Object.freeze({
        ...state,
        "navDrawerOpen": false
      });
    default:
      return state;
  }
};
