import { getLocaleMessages } from "./locales";

it("returns known languages", () => {
  const enMessages = getLocaleMessages("en");
  expect(enMessages.AGENCY_MENU_TITLE).toBe("Agency");
});
