import * as uuid from "node-uuid";
import * as SockJS from "sockjs-client";
import { Client, Frame, Message, over } from "stompjs";

// TODO put this back in!!
const disableWebSockets = false;

export const register = (registrations: Registration[]): Client => {
  if (!disableWebSockets) {
    const socket = new SockJS("/entity");
    const stompClient = over(socket);
    stompClient.connect({}, (frame: Frame) => {
      registrations.map((registration: Registration) => {
        return stompClient.subscribe(registration.destination, registration.callback, { "id": `${registration.type}_${uuid.v4()}` });
      });
    });
    return stompClient;
  } else {
    return {} as Client;
  }
};

export interface Registration {
  destination: string;
  type: string;
  callback(message: Message): void;
}
