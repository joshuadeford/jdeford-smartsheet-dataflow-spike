import { Request } from "rest";
import * as interceptor from "rest/interceptor";


/* If the URI is a URI Template per RFC 6570 (http://tools.ietf.org/html/rfc6570), trim out the template part */
export default interceptor({
     "request": (request: Request) => {
       const path = request.path || "";
       if (path.indexOf("{") === -1) {
         return request;
       } else {
         request.path = path.split("{")[0];
       return request;
       }
    }
});
