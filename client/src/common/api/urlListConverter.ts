import { HATEOSResource } from "./client";


export const uriListConverter = {
  /* Convert a single or array of resources into "URI1\nURI2\nURI3..." */
    "read"(str: string /*, opts */) {
      return str.split("\n");
    },
    // TODO pull this signature out into an interface
    "write"(resource: [HATEOSResource] | HATEOSResource) {
      // If this is an Array, extract the self URI and then join using a newline
      if (resource instanceof Array) {
        return resource.map((resourceItem) => {
          return resourceItem._links.self.href;
        }).join("\n");
      } else { // otherwise, just return the self URI
        return resource._links.self.href;
      }
    }

};

