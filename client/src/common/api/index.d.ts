
declare module "rest/mime/type/application/hal" {
  import { MIMEConverter } from "rest/mime/registry";

  const mimeInterceptor: MIMEConverter;

  export = mimeInterceptor;
}
