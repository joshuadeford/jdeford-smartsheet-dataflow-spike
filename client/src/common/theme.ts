import { Theme } from "@material-ui/core/es";
import { createMuiTheme } from "@material-ui/core/styles";
import createStyles from "@material-ui/core/styles/createStyles";


export const styles = (muiTheme: Theme) => createStyles({
  "gridTextFieldWrapper": {
    "margin": 3
  },
  "heading": {
    "fontSize": theme.typography.pxToRem(15),
    "fontWeight": theme.typography.fontWeightRegular
  },
  "primaryAppBar": {
  },
  "selectEmpty": {
    "marginTop": theme.spacing.unit * 2,
  },
  "wrapper": {
    "margin": 12
  }
});

const RED = "#679dd7";
const GRAY = "#58595B";
const CONTRAST_TEXT = "#FFFFFF";

export const theme = createMuiTheme({
  // Overrides the default theme values which can be found here https://material-ui.com/customization/default-theme/
  "overrides": {
    "MuiButton": {
      "root": {
      }
    },
    "MuiCard": {
      "root": {
        "margin": 6
      }
    },
    "MuiPaper": {
      "root": {
        "margin": 12,
        "padding": 12,
      }
    }
  },
  "palette": {
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    "contrastThreshold": 3,
    "error": {
      "main": RED
    },
    "grey": {
      "A200": GRAY
    },
    "primary": {
      "contrastText": CONTRAST_TEXT,
      "main": RED
    },
    "secondary": {
      "contrastText": RED,
      "main": CONTRAST_TEXT
    },
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    "tonalOffset": 0.2,
  }
});
