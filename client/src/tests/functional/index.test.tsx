

  it('for prod', () => {
    const root = document.createElement('div');
    root.setAttribute('id', 'root');
    // (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ = jest.fn((x: any) => { /* */ });
    document.body.appendChild(root);
    require('../../index');
  });


it('for dev', () => {
  const root = document.createElement('div');
  root.setAttribute('id', 'root');
  process.env = {
    "NODE_ENV": "development"
  };
  // (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ = jest.fn((x: any) => { /* */ });
  document.body.appendChild(root);
  require('../../index');
});

