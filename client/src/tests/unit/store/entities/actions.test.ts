import * as fetchMock from "fetch-mock";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { createEntity, deleteEntity, requestEntities, updateEntity } from "../../../../store/entities/actions";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

import { EntitySchema } from "../../../../store/entities/types";

const entitySchema: EntitySchema = {
  endpoint : "http://throwError",
  profile: {},
  properties : {},
  rel: "rel",
  title : "Nowhere",
  uri: "http://nowhere"
};

afterEach(() => {
  fetchMock.reset();
  fetchMock.restore();
});

// TODO test onSuccess and onError for error conditions
it("creates CREATE_ENTITY and ENTITY_CREATED when creating an entity has been done", () => {
  fetchMock.postOnce("/api/entities", {
    "body": [{ "typeName": "Mork FromOrk" }],
    "headers": { "content-type": "application/json" }
  });

  const store = mockStore({ "entities": [] });

  return store.dispatch(createEntity({ "id": "None", "schema": entitySchema, "uri": "uuu", "entityName": "som"  }, "colle") as any).then(() => {
    // return of async actions
    expect(store.getActions()).toMatchSnapshot();
  });
});


it("creates REQUEST_ENTITIES and RECEIVE_ENTITIES when fetching entities has been done", () => {
  fetchMock.getOnce("/api/entities", {
    "body": [{ "typeName": "Mork FromOrk" }],
    "headers": { "content-type": "application/json" }
  });

  const store = mockStore({ "entities": [] });

  return store.dispatch(requestEntities({
    "endpoint": "localhost",
    "profile": {},
    "properties": {},
    "rel": "",
    "title": "entityTitle",
    "uri": "Entity"
  }, { collectionName: "page", page: 1, size: 10, sort: "ASC" }) as any).then(() => {

    // return of async actions
    expect(store.getActions()).toMatchSnapshot();
  });
});

it("creates UPDATE_ENTITY and ENTITY_UPDATED when update and entity has been done", () => {
  fetchMock.patchOnce("/entities/1", {
    "body": [{ "typeName": "Mork FromOrk" }],
    "headers": { "content-type": "application/json" }
  });

  const store = mockStore({ "entities": [] });

  return store.dispatch(updateEntity({ "entityName": "", "uri": "Mork FromOrkee", "id": "1", "schema": entitySchema }) as any).then(({ /* */ }) => {
    // return of async actions
    expect(store.getActions()).toMatchSnapshot();
  });
});

it("creates DELETE_ENTITY and ENTITY_DELETED when deleting and entity has been done", () => {
  fetchMock.deleteOnce("/entities/1", {
    "body": [{ "typeName": "Mork FromOrkee" }],
    "headers": { "content-type": "application/json" }
  });

  const store = mockStore({ "entities": [] });

  // TODO get rid of onError and onSuccess
  return store.dispatch(deleteEntity({ "entityName": "", "uri": "Mork FromOrkee", "id": "1", "schema": entitySchema},
  ) as any).then(() => {
    // return of async actions
    expect(store.getActions()).toMatchSnapshot();
  });
});
