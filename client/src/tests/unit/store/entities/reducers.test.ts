import {entityStore} from "../../../../store/entities/reducers";
import {
  CREATE_ENTITY,
  DELETE_ENTITY,
  ENTITY_CREATED,
  ENTITY_DELETED,
  ENTITY_UPDATED, EntitySchema,
  RECEIVE_ENTITIES,
  REQUEST_ENTITIES,
  UPDATE_ENTITY
} from "../../../../store/entities/types";

const entitySchema: EntitySchema = {
  endpoint : "http://throwError",
  profile: {},
  properties : {},
  rel: "rel",
  title : "Nowhere",
  uri: "http://nowhere"
};

it("should return the initial state", () => {
  expect(entityStore(undefined, { "type": "None" })).toMatchSnapshot()
});

// TODO make a test that ensures state is NOT being modified
it("should handle CREATE_ENTITY", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": {}, "schemaByUri": {}, "schemaByRel": {} }, {
        "type": CREATE_ENTITY
      })
  ).toMatchSnapshot();
});

it("should handle ENTITY_CREATED", () => {
  expect(
      entityStore({ "isFetching": false, "schemaByRel": { }, "itemMapsByRel": {  }, "schemaByUri": {}  }, {
        "entity": { "entityName": "", "id": "1", "uri": "Fred", schema: entitySchema }, "type": ENTITY_CREATED
      })).toMatchSnapshot();
});

it("should handle REQUEST_ENTITIES", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": {}, "schemaByUri": {}, "schemaByRel": {}  }, {
        "type": REQUEST_ENTITIES
      })
  ).toMatchSnapshot();
});


it("should handle RECEIVE_ENTITIES", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": {}, "schemaByUri": {}, "schemaByRel": {} }, {
        "entities": [],
        "schema": entitySchema,
        "type": RECEIVE_ENTITIES
      })
  ).toMatchSnapshot();
});


it("should handle UPDATE_ENTITY", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": { }, "schemaByUri": {}, "schemaByRel": {} }, {
        "type": UPDATE_ENTITY
      })
  ).toMatchSnapshot();
});


it("should handle ENTITY_UPDATED", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": { }, "schemaByUri": {}, "schemaByRel": {} }, {
        "entity": { "entityName": "", "id": "1", "uri": "Freddie", "schema": entitySchema },
        "type": ENTITY_UPDATED
      })
  ).toMatchSnapshot();
});


it("should handle DELETE_ENTITY", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": { }, "schemaByUri": {}, "schemaByRel": {} }, {
        "type": DELETE_ENTITY
      })
  ).toMatchSnapshot();
});


it("should handle ENTITY_DELETED", () => {
  expect(
      entityStore({ "isFetching": false, "itemMapsByRel": { }, "schemaByUri": {}, "schemaByRel": {} }, {
        "entity": { "entityName": "", "id": "1", "uri": "Fred", "schema": entitySchema },
        "type": ENTITY_DELETED
      })
  ).toMatchSnapshot();
});
