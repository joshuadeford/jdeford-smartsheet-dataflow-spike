import {AppBar, IconButton, Tab, Tabs, Toolbar} from "@material-ui/core";
import { WithStyles, withStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import * as React from "react";
import {ChangeEvent} from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import { styles } from "../common/theme";
import { ErrorDialog } from "../components/ErrorDialog";
import { errorAcknowledged, errorAction, openNavDrawer } from "../store/app/actions";
import { AppStore } from "../store/app/reducers";
import { State } from "../store/State";
import SmartSheet from "./SmartSheet"

interface LayoutProps {
 openNavDrawer: typeof openNavDrawer;
 errorAcknowledged: typeof errorAcknowledged;
 errorAction: typeof errorAction;
 appStore: AppStore;
}

interface LayoutState {
  drawerOpen: boolean;
  didRender: boolean;
  tabValue: number;
}

export class Layout extends React.Component<WithStyles<typeof styles> & LayoutProps & InjectedIntlProps, LayoutState> {

  public state = {
    "didRender": false,
    "drawerOpen": false,
    "tabValue": 0
  };

  public componentDidMount() {
    if (!this.state.didRender) {
      this.setState({ "didRender": true });
    }
  }

  public shouldComponentUpdate() {
    return this.state.didRender;
  }

  public render() {
    const { appStore, intl } = this.props;
    return <React.Fragment>
      <AppBar position="static" >
        <Toolbar>
          <IconButton onClick={this.toggleDrawer()} color="inherit" aria-label="Menu">
            <MenuIcon/>
          </IconButton>
          <Tabs
              value={this.state.tabValue}
              onChange={this.handleTabChange}
              indicatorColor="primary"
              textColor="secondary"
              fullWidth={true}
          >
            <Tab label={ intl.formatMessage({"id": "SMART_SHEETS" }) } />
          </Tabs>
        </Toolbar>
      </AppBar>

      <SmartSheet sheetId="3222748173494148" drawerOpen={appStore.navDrawerOpen} toggleDrawer={this.toggleDrawer()}/>

      <ErrorDialog isOpen={!!appStore.messageKey}
                   handleClose={this.closeError}
                   intl={intl}
                   messageKey={appStore.messageKey}
                   details={appStore.details || ""}/>

    </React.Fragment>;
  }

  private toggleDrawer = () => (event: any): void => {
    const { openNavDrawer: openNavDrawerThunk } = this.props;

    openNavDrawerThunk();
  }

  private handleTabChange = (event: ChangeEvent, value: number) => {
    this.setState({ tabValue: value });
  };


  private closeError = () => {
    const { errorAcknowledged: ack } = this.props;
    ack();
  }
}


function mapStateToProps(state: State) {
  const { appStore } = state;

  return {
    "appStore": appStore
  };
}


export default connect(mapStateToProps, { openNavDrawer, errorAction, errorAcknowledged })(withStyles(styles)(injectIntl(Layout)));

