import {Button, Drawer, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import * as AWS from 'aws-sdk';
import * as React from "react";
import {Client} from "stompjs";
import {styles} from "../common/theme";

import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
// import { Socket, socket as zmqSocket } from 'zeromq';
import * as webSocket from "../common/util/websocketListener";


// const API_ROOT = "https://api.smartsheet.com/2.0";
const API_ROOT = "https://0e94cemxwh.execute-api.us-east-1.amazonaws.com/jdeford-dev";
// const SHEET_ID = "3222748173494148";

// Cognito_nbcusqsUnauth_Role
// Cognito_nbcusqsAuth_Role
// ROLE ARN
// arn:aws:iam::683553748519:role/Cognito_nbcusqsUnauth_Role
// Identity pool id
// us-east-1:44df4ecd-d8d2-4425-952a-3f9d53fc2f5a
// Identity pool arn
// arn:aws:cognito-identity:us-east-1:683553748519:identitypool/us-east-1:44df4ecd-d8d2-4425-952a-3f9d53fc2f5a
// SQS Queueu Name
// testsheetQ
// Queue ARN
// arn:aws:sqs:us-east-1:683553748519:testsheetQ
// Queue URL
// https://sqs.us-east-1.amazonaws.com/683553748519/testsheetQ
AWS.config.region = 'us-east-1';
AWS.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId: 'us-east-1:44df4ecd-d8d2-4425-952a-3f9d53fc2f5a'});

export const SHEET_ID = "3222748173494148";
export const SELECTION_INPUTS_SHEET_ID = "7563778993743748";
export const RETAIL_CONFIG_SHEET_ID = "7282304017033092";
export const TABLES_CONFIG_SHEET_ID = "3060179366373252";
export const ACCESS_CONTROL_CONFIG_SHEET_ID = "2778704389662596";

export const FIELD_CONFIG_START_IDENTIFIER = "{Inputs}"
export const SHOPPING_CART_SECTION_IDENTIFIER = "{Shopping Cart}"

const userMap = {
  "1966830446241668": "Amy Meckes",
  "2764378492692356": "Jesse Carrigan",
  "3400274170668932": "Casey Kenley",
  "6054102519048068": "Jeremy Jenkins",
  "6338890794592132": "Josh DeFord",
  "6507578319497092": "PJ Tatano",
  "824118426789764":"Kristian DeVito",
  "8342325266016132": "Christophe Ponsart",
  "8821258679805828": "Jeff Schroeder",
  "8872165584988036": "Rajib Khan"
}

console.log(userMap);

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

const queueURL = "https://sqs.us-east-1.amazonaws.com/683553748519/testsheetQ";

const queueParams = {
  AttributeNames: [
    "SentTimestamp"
  ],
  MaxNumberOfMessages: 10,
  MessageAttributeNames: [
    "All"
  ],
  QueueUrl: queueURL,
  VisibilityTimeout: 120,// The queue should have the max ttl set to 60 so this client won't get duplicate messages
  WaitTimeSeconds: 20// long polling
};

/* *********************************** */
/* ********* UI Config *************** */
/* *********************************** */
export interface UIConfig extends MenuConfig {
  childrenMenuConfigs: MenuConfig[]
}

export interface MenuConfig {
  labelKey: string
  childrenMenuConfigs: MenuConfig[]
  rowId: number
  sectionCreateConfigs: SubstitutionFieldConfig[]
  sectionEditConfigs: FormSectionConfig[]
}

// export interface InputFieldConfig {
// }

export interface FormSectionConfig {
  titleKey?: string
  formConfigs: SubstitutionFieldConfig[]
}


export interface SubstitutionFieldConfig {
  fieldName: string
  type: string,
  sheetKeys: string
  rowKeyId: string
  required: boolean,
  defaultValue: string,
  substitutionVariable: string,
  helpTextKey: string
  tooltipKey: string
  securityTag: string
  refId: string
}

const columnNameFieldMap = {
  "Default/Required": "required",
  "Help Text": "helpTextKey",
  "Hierarchy": "fieldName",
  "RefID": "refId",
  "ReferenceID": "rowKeyId",
  "Security Tag": "securityTag",
  "Sheet Keys": "sheetKeys",
  "Tag to Replace": "substitutionVariable",
  "Tooltip": "tooltipKey",
  "Type": "type"
}
/* *********************************** */



/* ************************************** */
/* ********* Smart Sheets *************** */
/* ************************************** */
export interface SmartSheetPayload {
  accessLevel: string
  cellImageUploadEnabled: boolean
  columns: SmartSheetColumn[],
  createdAt: string
  dependenciesEnabled: boolean
  effectiveAttachmentOptions: string[]
  ganttEnabled: boolean
  id: number
  modifiedAt: string
  name: string
  permalink: string
  resourceManagementEnabled: boolean
  rows: SmartSheetRow[]
  totalRowCount: number
  userSettings: {}
  version: number
  workspace: {}

}
export interface SmartSheetEvent {
  eventType: string
  id: number
  objectType: string
  timestamp: string
  userId: number
}

export interface SmartSheetColumn {
  id: number
  index: number
  primary: true
  title: string
  type: string
  validation: false
  version: number
  width: number
}
export interface SmartSheetRow {
  cells: SmartSheetCell[]
  parentId?: number,
  createdAt: string
  expanded: boolean
  id: number
  modifiedAt:  string
  rowNumber: number
  siblingId?: number
}

export interface SmartSheetCell {
  columnId: number
  displayValue?: string
  value?: string
}
/* ************************************** */

export interface RowChange {
  firstCellValue?: string
  type: string
  username: string
  row: SmartSheetRow
}


/* ***************************************** */
/* ********** Internal State *************** */
/* ***************************************** */
export interface SheetData {
  columnMap: {[id: number]: string}
  rows: Row[]
}

export interface Row {
  parentId?: number,
  id: number,
  columns: {[columnName: string]: SmartSheetCell}
}
/* ***************************************** */


/* *********************************** */
/* ********* Component *************** */
/* *********************************** */
export interface SmartSheetProps extends WithStyles<typeof styles> {
  sheetId: string,
  drawerOpen: boolean,
  toggleDrawer: (event: any) => void
}


export interface SmartSheetState {
  "rowChanges": {
    [id: string]: RowChange
  }
  "openMenuItems": {[label: string]: boolean}
  "uiConfig"?: UIConfig
  "rowGroups": any
  "sheetRows": {
    [id: string]: SmartSheetRow
  }
}

class SmartSheet extends React.Component<SmartSheetProps, SmartSheetState> {

  private webSocketClient: Client;
  // private zmqClient: Socket;

  constructor(props: SmartSheetProps) {
    super(props);

    this.state = {
      "openMenuItems": {},
      "rowChanges": {},
      "rowGroups": {},
      "sheetRows": {}
    };
  }
  public componentDidMount() {
    this.fetchSheets();
    this.manageSocketClients();
  }

  public componentWillUnmount() {
    if (this.webSocketClient) {
      this.webSocketClient.disconnect(() => { /* **/ } );
    }
  }

  public manageSocketClients() {
    // this.listenToWebSocket();

    this.listenToQueue();
    this.parseInputConfig();

    // this.zmqClient = zmqSocket('sub');
    // this.zmqClient.connect('tcp://127.0.0.1:3000');
    // this.zmqClient.subscribe("/topic/sheetUpdate");
    // this.zmqClient.on("message", (data) => {
      // alert("0MQ Message received " + JSON.stringify(data));
    // });
    // alert('0MQ Subscriber connected');
  }

  public listenToWebSocket() {
    if (this.webSocketClient) {
      this.webSocketClient.disconnect(() => { /* required but not needed */
      });
    }

    // These are the same prefixes used in BaseEntityListener.java
    const topicTypes = ["new", "updated", "deleted", "linked", "unlinked"];

    const registrations = topicTypes.map((type: string) => {
      return {
        "callback": this.handlewebSocketMessage(type),
        "destination": `/topic/sheetUpdate`,
        "type": `sheet`
      };
    });

    this.webSocketClient = webSocket.register(registrations);
  }

  public async parseInputConfig() {
    const request: RequestInit = {
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      "method": "GET",
    };
    const response: Response = await fetch(`${API_ROOT}/sheet?sheetId=${SELECTION_INPUTS_SHEET_ID}`, request);
    console.log(JSON.stringify(response));

    const sheetPayload: SmartSheetPayload = await response.json() as SmartSheetPayload;
    const sheetData: SheetData = this.convertToSheetData(sheetPayload);
    const uiConfig = this.createUIConfig(sheetData);
    this.setState({"uiConfig": uiConfig});
    // console.log(JSON.stringify(uiConfig));
  }

  public createUIConfig(sheetData: SheetData): UIConfig {
    // keep track of the menu configs by row id to look up
    // parents by id and properly create the menu hierarchy
    const menuConfigMap: {[id: number]: MenuConfig} = {};
    const idHierarchy: number[] = [];
    // const nameHierarchyForDebugging: string[] = [];

    const uiConfig: UIConfig = {
      childrenMenuConfigs: [],
      labelKey: "UI",
      rowId: 0,
      sectionCreateConfigs: [],
      sectionEditConfigs: []
    }

    let rowIndex = 1;
    // let doingInputs = false;
    // If a row has the current "{Inputs}" row as it id that's how you know
    // you're still doing inputs for a form
    // let currentInputsRowId = 0;
    // let currentFormConfigs: SubstitutionFieldConfig[] = [];
    // When this thing is not null the rows represent input fields until
    // a condition exists that input fields are done at the current level
    let currentInputFieldConfigs: SubstitutionFieldConfig[] | null = [];
    let lastRowId = 0;// Keep track of the position in the hierarchy
    let lastRowParentId = 0;// Keep track of the position in the hierarchy
    let fieldConfigStartRowId = 0;
    let shoppingCartConfigStartRowId = 0;
    let insideShoppingCartHierarchy = false;
    let currentRow: Row = sheetData.rows[rowIndex];
    while (currentRow) {
      rowIndex++;
      // The value in this cell represent the semantics around
      // How the menus/submenus and sections are setup
      const hierarchyCellValue = currentRow.columns.Hierarchy.value;
      if (hierarchyCellValue) {
        const currentRowParentId = currentRow.parentId;
        if (lastRowParentId === currentRowParentId) {
          // There was  no movement down the hierarchy.  Just replace the last id in the hierarchy
          // with the current id
          idHierarchy.splice(idHierarchy.length - 1, 1, currentRow.id)
          // nameHierarchyForDebugging.splice(idHierarchy.length - 1, 1, hierarchyCellValue)
        } else if (lastRowId === currentRowParentId) {
          // There was movement down to the next level of the heirarchy.  Add the current
          // id to the hierarchy stack
          idHierarchy.push(currentRow.id)
          // nameHierarchyForDebugging.push(hierarchyCellValue)
        } else {
          // There was a jump out of the hierchy up to another level
          // Determine how many levels up.  Remove them from the hierarchy and
          // put the current id onto the stack.
          let hierarchyLevelCount = -1;// The number of levels up that have been left.
          let nextLevelId = 0;
          // let poppedOutOfShoppingCart = false;
          for (let i=idHierarchy.length - 1; i >= 0 && currentRowParentId !== nextLevelId; i--) {
            hierarchyLevelCount++
            nextLevelId = idHierarchy[i];
            // Also track if the level of the shopping cart is being left
            if (nextLevelId === shoppingCartConfigStartRowId && currentRowParentId !== shoppingCartConfigStartRowId) {
              // poppedOutOfShoppingCart = true;
              insideShoppingCartHierarchy = false;
              currentInputFieldConfigs = null;
            }
          }
          if (!currentRowParentId) {
            hierarchyLevelCount = idHierarchy.length;
          }
          if (hierarchyLevelCount >= 0) {
            const start = idHierarchy.length - hierarchyLevelCount;
            idHierarchy.splice(start);
            // nameHierarchyForDebugging.splice(start);
          }
          idHierarchy.push(currentRow.id);
          // nameHierarchyForDebugging.push(hierarchyCellValue);
          // if (poppedOutOfShoppingCart) {
            // console.log("Popping out of shopping cart " + JSON.stringify(nameHierarchyForDebugging.join(", ")));
          // }
        }
        lastRowParentId = currentRowParentId as number;

        if (currentRowParentId !== fieldConfigStartRowId) {
          // The parent row id of this row not being the fieldConfigRowId (The one with the value of "{Inputs}")
          // indicates that fieldConfigs are no longer being done.
          currentInputFieldConfigs = null;
        }

        if (currentInputFieldConfigs) {
          const inputFieldConfig: SubstitutionFieldConfig = {} as SubstitutionFieldConfig;
          for (const columnName in columnNameFieldMap) {
            if (columnNameFieldMap.hasOwnProperty(columnName)) {
              inputFieldConfig[columnNameFieldMap[columnName]] = currentRow.columns[columnName].value;
            }
          }
          currentInputFieldConfigs.push(inputFieldConfig)
        }

        if (hierarchyCellValue !== "{}" && hierarchyCellValue !== SELECTION_INPUTS_SHEET_ID && !insideShoppingCartHierarchy) {
          // Just skip all the shopping cart stuff
          if (hierarchyCellValue === FIELD_CONFIG_START_IDENTIFIER) {
            fieldConfigStartRowId = currentRow.id;
            currentInputFieldConfigs = [];
            const menuConfig = this.getParentMenuConfig(uiConfig, menuConfigMap, currentRowParentId, idHierarchy);
            menuConfig.sectionCreateConfigs = currentInputFieldConfigs;
            // currentMenuConfig.sectionCreateConfigs.push({});
          } else if (hierarchyCellValue === SHOPPING_CART_SECTION_IDENTIFIER) {
            // console.log("GOING INTO shopping cart hierarchy " + JSON.stringify(nameHierarchyForDebugging.join("\n")));
            insideShoppingCartHierarchy = true;
            shoppingCartConfigStartRowId = currentRow.id;
          } else if (!currentInputFieldConfigs) {
            // Top level rows represent top level menus
            const menuConfig: MenuConfig = {
              childrenMenuConfigs: [],
              labelKey: hierarchyCellValue,
              rowId: currentRow.id,
              sectionCreateConfigs: [],
              sectionEditConfigs: []
            }
            const menuConfigs = this.getChildrenMenuConfigs(uiConfig, menuConfigMap, currentRowParentId, idHierarchy);
            menuConfigMap[currentRow.id] = menuConfig;
            menuConfigs.push(menuConfig);
          }
        }

        // console.log(hierarchyCellValue);
        // console.log(JSON.stringify(menuConfigMap));
      }
      lastRowId = currentRow.id;
      currentRow = sheetData.rows[rowIndex];
    }

    return uiConfig;
  }

  public getChildrenMenuConfigs(uiConfig: UIConfig, menuConfigMap: {}, currentRowParentId: number | undefined, idHierarchy: number[]): MenuConfig[] {
    return this.getParentMenuConfig(uiConfig, menuConfigMap, currentRowParentId, idHierarchy).childrenMenuConfigs;
  }

  public getParentMenuConfig(uiConfig: UIConfig, menuConfigMap: {}, currentRowParentId: number | undefined, idHierarchy: number[]): MenuConfig {
    if (currentRowParentId) {
      // Search up the id hierarchy for a parent that has a menu
      // and add it
      for (let i = idHierarchy.length - 1; i > 0; --i) {
        const rowParentId = idHierarchy[i - 1];
        const parentMenuConfig: MenuConfig = menuConfigMap[rowParentId];
        if (parentMenuConfig) {
          return parentMenuConfig;
        }
      }
    }
    return uiConfig;
  }

  public convertToSheetData(sheetPayload: SmartSheetPayload): SheetData {
    const columnMap = {};
    for (const column of sheetPayload.columns) {
      columnMap[column.id] = column.title;
    }

    const sheetData = {rows: [], columnMap} as SheetData;
    for (const smartSheetRow of sheetPayload.rows) {
      const rowData: Row = {id: smartSheetRow.id, parentId: smartSheetRow.parentId, columns: {}}
      for (const cell of smartSheetRow.cells) {
        const columnTitle = columnMap[cell.columnId];
        rowData.columns[columnTitle] = cell;
      }
      sheetData.rows.push(rowData);
    }

    return sheetData
  }

  public async listenToQueue() {
    while (true) {
      try {
        const messageResult = await sqs.receiveMessage(queueParams).promise();
        const messages = messageResult.Messages;
        if (messages) {
          console.log("Messages: " + messages.length);
          for (const message of messages) {
            if (message.Body) {
              const eventPayload = JSON.parse(message.Body);
              const rowMap: {[id: string]: SmartSheetRow} = eventPayload.modifiedRows.reduce((acc: {[id: string]: SmartSheetRow}, current: SmartSheetRow) => {
                acc[current.id] = current;
                return acc;
              }, {})
              const events = eventPayload.events as SmartSheetEvent[];
              const rowChanges = { ...this.state.rowChanges }
              console.log("Processing " + events.length);
              for (const event of events) {
                if (event.objectType === "row") {
                  const row = rowMap[event.id];

                  const deleted = event.eventType === "deleted";
                  const created = event.eventType === "created";
                  console.log("Event to test " + event.eventType);
                  if (created || deleted) {
                    const firstCellValue = deleted ? (event.id + "") : row.cells[0].value;
                    if (created && !firstCellValue) {
                      console.log("A row was created with no first value: " + JSON.stringify(events));
                    }
                    if (deleted || firstCellValue) {
                      rowChanges[event.id] = {
                        firstCellValue,
                        row: rowMap[event.id],
                        type: event.eventType,
                        username: userMap[event.userId]
                      }
                    }
                  }
                }
              }
              this.setState({ rowChanges } );
              console.log("Received message " + JSON.stringify(eventPayload));
            }
          }
        }
      } catch (e) {
        alert("Error processing message " + e.message);
      }
    }
  }

  public handleMenuClick = (menuConfig: MenuConfig) => () => {
    this.state.openMenuItems[menuConfig.rowId] = !this.state.openMenuItems[menuConfig.rowId];
    this.setState({openMenuItems:  this.state.openMenuItems});

    // if () {

    // }
  }

  public getMenuList(parentMenuConfig: MenuConfig, topLevel: boolean = true) {
    const { classes } = this.props;
    const menuList = [];
    if (parentMenuConfig) {
      const menuConfigs = parentMenuConfig.childrenMenuConfigs;
      for (const menuItemConfig of menuConfigs) {
        if(menuItemConfig.childrenMenuConfigs) {
          const hasChildren = menuItemConfig.childrenMenuConfigs.length > 0;
          menuList.push(<React.Fragment key={menuItemConfig.labelKey}>
                <ListItem key={menuItemConfig.labelKey + "item"} button={false} onClick={this.handleMenuClick(menuItemConfig)}>
                  <ListItemText className={topLevel ? classes.topLevelItem : classes.nestedItem} primary={menuItemConfig.labelKey}/>
                  {hasChildren ? this.state.openMenuItems[menuItemConfig.rowId] ? <ExpandLess/> : <ExpandMore/> : null}
                </ListItem>
                {hasChildren ?
                <Collapse key={menuItemConfig.labelKey + "collapse"} in={this.state.openMenuItems[menuItemConfig.rowId]} timeout="auto" unmountOnExit={true}>
                  <List component="div" disablePadding={true}>
                    {hasChildren ? this.getMenuList(menuItemConfig, false) : null}
                  </List>
                </Collapse>
                : null }
              </React.Fragment>
          );
         }

      }
    }
    return menuList;
  }

  public render () {
      const { classes, toggleDrawer } = this.props;

    const sideList = (
      <div >
        { this.state.uiConfig ? this.getMenuList(this.state.uiConfig) : null}
      </div>
    );

      return <div className={classes.wrapper}>
        <div>
          <Drawer open={this.props.drawerOpen} >
            <div tabIndex={0}
                 role="button"
                 onClick={toggleDrawer}
                 onKeyDown={toggleDrawer}
            >
                    {sideList}
              </div>
            </Drawer>
          <div>
            <Typography variant={"h4"} >Changes</Typography>
            { this.getUpdates() }
          </div>
          <div>
            <Typography variant={"h4"} >Sheet Rows</Typography>
            { this.getSheetLinks() }
          </div>
        </div>
      </div>;
  }

  private handlewebSocketMessage = (type: string) => () => {
    alert("Received a webSocket message");
  }

  private handleClickDestroy = (rowId: string) => {
    return async () => {
      let method: string;
      const changeType = this.state.rowChanges[rowId].type;
      if (changeType === "deleted") {
        method = "PATCH";
      } else {
        method = "DELETE";
      }
      const request: RequestInit = {
        "headers": {
          "Accept": "application/json",
          "Content-Type": "application/json",
        },
        "method": method,
      };
      if (changeType === "deleted") {
        request.body = JSON.stringify(this.state.sheetRows[rowId]);
      }

      const response: Response = await fetch(`${API_ROOT}/sheet?sheetId=${SHEET_ID}&rowId=${rowId}`, request);
      const sheetPayload = await response.json() as SmartSheetPayload;
      this.setStateFromSheet(sheetPayload);
    }
  };

  private handleClickAccept = (rowId: string) => {
    return this.fetchSheets;
  }

  private getUpdates = ()  => {
    const elements = [];
    const changes = this.state.rowChanges;
    if (changes) {
      for (const rowId in changes) {
        if (changes.hasOwnProperty(rowId)) {
          const change = changes[rowId];
          elements.push(<Paper key={rowId}>
            <div>{change.username} {change.type} a row {change.firstCellValue}</div>
            <Button onClick={this.handleClickDestroy(rowId)}>Destroy Change</Button> <Button onClick={this.handleClickAccept(rowId)}>Accept Change</Button>
          </Paper>);
        }
      }
    }
    return elements;
  }

  private getSheetLinks = ()  => {
    const elements = [];
    if (this.state.rowGroups) {
      for (const rowGroup in this.state.rowGroups) {
        if (this.state.rowGroups.hasOwnProperty(rowGroup)) {
          elements.push(<Paper key={rowGroup}>{rowGroup} has {this.state.rowGroups[rowGroup]} rows</Paper>);
        }
      }
    }
    return elements;
  };


  private fetchSheets = async (): Promise<void> => {
    try {
      const response: Response = await fetch(`${API_ROOT}/sheet`, {
        "headers": {
          "Accept": "application/json",
          "Content-Type": "application/json",
        },
        "method": "GET"
      });
      const sheetPayload = await response.json();
      this.setStateFromSheet(sheetPayload);
    } catch (error) {
      alert(`Couldn't getch the sheets ${error.message}`)
    }
  };

  private setStateFromSheet(sheetPayload: SmartSheetPayload) {
    const rows = sheetPayload.rows;
    const groups = {};
    const sheetRows = {};
    rows.forEach((row: SmartSheetRow) => {
      sheetRows[row.id] = row;
      const cells = row.cells as SmartSheetCell[];
      const firstCellValue = cells[0].value;
      if (firstCellValue) {
        if (!groups[firstCellValue]) {
          groups[firstCellValue] = 0;
        }
        groups[firstCellValue]++;
      }
    });
    this.setState({sheetRows, rowGroups: groups, rowChanges: {}});
  }
}


// function mapStateToProps(state: State, ownProps: SmartSheetProps) {
  // const { appStore } = state;
  // const { match } = ownProps;
//
  // return {
    // "appStore": appStore,
    // "match": match
  // };
// }

export default (withStyles(styles)(SmartSheet));
// export default connect(
    // mapStateToProps,
    // // This is the magic that allows the dispatch 'thunk' actions to be created and puts them in props
    // { /* */ }
 // )(withStyles(styles)(withRouter(injectIntl(SmartSheet))));
