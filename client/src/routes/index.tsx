import * as React from "react";
import { Route, Switch } from "react-router";
import NoMatch from "../components/NoMatch";
import Layout from "../containers/Layout";
import SmartSheet from "../containers/SmartSheet";

const routes = (
  <div>
    <Layout />
    <Switch>
      <Route path="/sheets/:sheetId" component={SmartSheet} />
      <Route component={NoMatch} />
    </Switch>
  </div>
);

export default routes;
