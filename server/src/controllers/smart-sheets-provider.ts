import {registerWebhook, respondToWebhookChallenge} from "./smart-sheet-hook-registrar";

import {Request, Response, Router} from 'express';
import fetch, {Response as FetchResponse} from "node-fetch";

import {
  API_ROOT,
  DEFAULT_SHEET_ID,
  SHEETS_API_TOKEN,
  SMART_SHEETS_SHA_256_HEADER,
  SMART_SHEETS_WEB_HOOK_CHALLENGE_HEADER,
  smartsheet, WEBHOOK_CALLBACK_URL
} from "./smart-sheets";

const AWS = require('aws-sdk');
import debug from 'debug';
import {sendEvent} from "./sqs-processor";
const log = debug("SmartSheets");
const dynamoDb = new AWS.DynamoDB.DocumentClient();

export const SHEET_EVENT_TABLE = process.env.SHEET_EVENT_TABLE;
// Not really sure how to kmow its an event


interface SmartSheetCell {
  columnId: number
  displayValue?: string
  value: string
}

export const addRow = async (req: Request, res: Response) => {
  const sheetId = req.query.sheetId || DEFAULT_SHEET_ID;
  try {
    log("About to process row");
    const row: any = req.body;
    log("Processing row " + JSON.stringify(row));
    const siblingId = row.siblingId;

    const cells: SmartSheetCell[] = [];
    const newRow = {"toTop": true, cells};

    for (const cell of row.cells) {
      if (typeof cell.value !== "undefined") {
        newRow.cells.push({
          "columnId": cell.columnId,
          "value": cell.value
        });
      }
    }

    var options = {
      body: newRow,
      sheetId: sheetId
    };

    log("Posting row: " + JSON.stringify(options));
    await smartsheet.sheets.addRows(options)
  } catch (e) {
    log("Couldn't add row " + e.message);
  }
  return await getRawSheetData(sheetId);
}

// export const updatRow = async (req: Request, res: Response) => {
//
// }
export const deleteRow = async (req: Request, res: Response) => {
  const sheetId = req.query.sheetId || DEFAULT_SHEET_ID;
  try {
    var options = {
      sheetId: sheetId,
      rowId: req.query["rowId"]
    };

    await smartsheet.sheets.deleteRow(options);
  } catch (e) {
    log("Couldn't delete row " + e.message);
  }
  return await getRawSheetData(sheetId);
}

export const getSheet = async (req: Request, res: Response) => {
  try {
    const sheetId = req.query.sheetId || DEFAULT_SHEET_ID;
    log(`Calling sheet. If you aren't getting webhook events make sure the webhook is registered by reloading the following page a few times ${WEBHOOK_CALLBACK_URL}?register=true`);
    const challengeHeader: string = req.get(SMART_SHEETS_WEB_HOOK_CHALLENGE_HEADER) as string;
    const eventHeader: string = req.get(SMART_SHEETS_SHA_256_HEADER) as string;

    if (challengeHeader) {
      return respondToWebhookChallenge(challengeHeader, res);
    } else if (req.query.register) {
      const result = await registerWebhook();
      return result;
    } else if (eventHeader) {
      return await respondToEvents(sheetId, req.body);
    } else {
      return await getRawSheetData(sheetId);
    }
  } catch (e) {
    throw new Error("Error processing getSheet " + e.message);
  }
}

const respondToEvents = async (sheetId: string, event: any) => {
  const result = {"status": "all good"};
  try {
    console.log("Attempting to respond to event " + JSON.stringify(event));
    //const eventBody = JSON.parse(eventText);
    console.log(`Responding to event ${event}`);
    if (event.type === "Buffer") {
      //This block of code appears to be unused.  I'm 100% certain that on 11/13/2018
      //It was being used because a buffer was coming in somehow.  However, it appears that is
      //no longer the case.  It could have been an AWS API Gateway thing?
      log("Parsing the buffered data " + JSON.stringify(event.data));
      const byteBuffer = Buffer.from(event.data);
      log("Parsed the buffered data");
      const eventData = byteBuffer.toString();
      log("Decoded the data " + eventData);
      const eventsPayload = JSON.parse(eventData);
      log("Parsed the eventsPayload data " + JSON.stringify(eventsPayload));
      await processEvent(sheetId, eventsPayload);
      log("Successfully stored eventsPayload: " + JSON.stringify(eventsPayload));
    } else if (event.nonce) {
      await processEvent(sheetId, event);
    } else {
    }
  } catch (e) {
    log("Couldn't process event " + e.message);
    throw new Error(typeof event + "type Event could not be processed: ");
  }
  return result;
}

const processEvent = async (sheetId: string, eventsPayload: any) => {
  await createEventPayload(sheetId, eventsPayload);
  let lastWasAnUpdatedRowId = false;
    for (const rowEvent of eventsPayload.events) {
      if (lastWasAnUpdatedRowId && rowEvent.eventType == "deleted") {
        rowEvent.id = lastWasAnUpdatedRowId;
      }
      let thisUpdatedRowId = null;
      if (rowEvent.objectType === "row" && rowEvent.eventType == "updated") {
        thisUpdatedRowId = rowEvent.id;
      }
      lastWasAnUpdatedRowId = thisUpdatedRowId;
    }
  try {
    await sendEvent(eventsPayload);
  } catch (e) {
    log("Error sending event: " + e.message);
  }
  try {
    await storeEvent(eventsPayload);
  } catch (e) {
    log("Error storing event: " + e.message);
  }
}


const createEventPayload = async (sheetId: string, eventsPayload: any) => {
  const rows = await getRows(sheetId, eventsPayload);
  eventsPayload.eventId = eventsPayload.nonce;
  eventsPayload.modifiedRows = rows;
}

/**
 * Store the event in a dynamo db table
 * @param eventsPayload
 */
const storeEvent = async (eventsPayload: any) => {
  try {
    const params = {
      Item: eventsPayload,
      TableName: SHEET_EVENT_TABLE
    };

    const dataKeyResult = await dynamoDb.put(params).promise();
  } catch (e) {
    log("Could not store event " + e.message);
    throw new Error(e.message);
  }
}

/**
 * Extract the row ids from the update eventsPayload then fetch them from
 * smartsheets
 */
const getRows = async (sheetId: string, eventsPayload: any) => {
  try {
    const rowIds: Set<string> = new Set();
    for (const event of eventsPayload.events) {
      if (event.objectType === "row") {
        rowIds.add(event.id)
      }
    }

    const sheetExcerpt: any = await getRawSheetData(sheetId, Array.from(rowIds));
    const rows = sheetExcerpt.rows ? sheetExcerpt.rows : [];
    log("Got " + rows.length + " rows" + JSON.stringify(rows));
    return rows;
  } catch (e) {
    log("Couldn't fetch rows " + e.message);
    throw new Error(e.getMessage());
  }
}

const getRawSheetData = async (sheetId: string, rowIds?: string[]) => {
  let sheetData = "";
  try {
    let resource = `${API_ROOT}/sheets/${sheetId}`;
    if(rowIds) {
      resource += `?rowIds=${rowIds.join(",")}`;
    }

    log("Fetching sheet data from " + resource);
    const response: FetchResponse = await fetch(resource, {
      "headers": {
        "Accept": "application/json",
        "Authorization": `Bearer ${SHEETS_API_TOKEN}`,
        "Content-Type": "application/json",
      },
      "method": "GET"
    });
    sheetData = await response.json();
  } catch (e) {
    throw new Error("Error getting raw smartSheets data " + e.message);
  }
  return sheetData;
}

