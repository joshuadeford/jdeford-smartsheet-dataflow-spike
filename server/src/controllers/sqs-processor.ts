import * as AWS from 'aws-sdk';
import {SendMessageRequest} from 'aws-sdk/clients/sqs';

// We should probably be dynamically creating queues
// export const QUEUE_URL = "https://sqs.us-east-1.amazonaws.com/683553748519/smartsheet-webhook-queue";
export const QUEUE_URL = "https://sqs.us-east-1.amazonaws.com/683553748519/testsheetQ";

export const messagingState = {
  sendingMessages: false,
  producerId: (Math.random() * 10000000).toFixed(0),
  waitingForMessages: false,
  messageAccumulator: [],
  queueUrl: QUEUE_URL
}

import debug from 'debug';
const log = debug("SQS");

AWS.config.region = 'us-east-1'; // Region



AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: 'us-east-1:44df4ecd-d8d2-4425-952a-3f9d53fc2f5a',
  // IdentityId: 'us-east-1:efdb08c0-04b3-4ba1-97dc-da63c5c2e594',
  LoginId: 'example@gmail.com',
  RoleArn: 'arn:aws:iam::683553748519:role/Cognito_nbcusqsUnauth_Role'
});

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});


export const sendEvent = async (message: any) => {
  log("Sending message to sqs " + JSON.stringify(message));
  try {
    const params: SendMessageRequest = {
      QueueUrl: messagingState.queueUrl,
      DelaySeconds: 0,
      // MessageGroupId: messagingState.producerId, This is FIFO only
      MessageBody: JSON.stringify(message)
    };

    const data = await sqs.sendMessage(params).promise();
  } catch (e) {
    log("Couldn't post to SQS Queue " + e.message);
  }
  log("Receive ack from sqs ");
}
