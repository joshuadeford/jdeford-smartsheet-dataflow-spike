
import { Request, Response, Router } from 'express';
const router: Router = Router();



router.get('/api/hello', (req, res) => {
  res.send({ express: 'Hello From Express' });
});
router.post('/api/world', (req, res) => {
  // console.log(req.body);
  res.send(
      `I received your POST request. This is what you sent me: ${req.body.post}`,
  );
});

router.get('/:name', (req: Request, res: Response) => {
  // Extract the name from the request parameters
  const { name } = req.params;

  // Greet the given name
  res.send(`Hello, ${name}`);
});

export const WelcomeController: Router = router;

