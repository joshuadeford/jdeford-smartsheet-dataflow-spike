export const API_ROOT = "https://api.smartsheet.com/2.0";
export const SHEETS_API_TOKEN = "j2v8hu7kt4ruz4eyrxcixxi1kd";
export const DEFAULT_SHEET_ID = "3222748173494148";
export const WEBHOOK_CALLBACK_URL = `${process.env.GW_URL}/sheet`;
export const SMART_SHEETS_WEB_HOOK_CHALLENGE_HEADER = "Smartsheet-Hook-Challenge";
export const SMART_SHEETS_WEB_HOOK_RESPONSE_HEADER = "Smartsheet-Hook-Response";
export const SMART_SHEETS_SHA_256_HEADER = "smartsheet-hmac-sha256";
const client = require('smartsheet');
export const smartsheet = client.createClient({
  accessToken: SHEETS_API_TOKEN,
  logLevel: 'info'
});


