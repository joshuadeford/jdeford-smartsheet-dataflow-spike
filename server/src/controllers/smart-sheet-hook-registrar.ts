import debug from 'debug';
import {Response} from "express";
import {
  DEFAULT_SHEET_ID,
  SMART_SHEETS_WEB_HOOK_RESPONSE_HEADER,
  smartsheet,
  WEBHOOK_CALLBACK_URL
} from "./smart-sheets";
const client = require('smartsheet');

const log = debug("SmartSheets");

export const postWebhookUpdate = async (webhook: any) => {
  const updateOptions = {
    body: {enabled: true},
    webhookId: webhook.id
  };

  // This will await until smartsheets issues a challenge to this endpoint and gets
  // the response
  log(`Updating webhook ${WEBHOOK_CALLBACK_URL} ${JSON.stringify(webhook)}`);
  const updatedWebhook = await smartsheet.webhooks.updateWebhook(updateOptions)
  log("Fetched sheet data from " + JSON.stringify(updatedWebhook));
}

export const registerWebhook = async () => {
  log("Going to look for an existing webhook.");
  const foundWebhook = await getRegisteredWebhook(WEBHOOK_CALLBACK_URL);
  let result;
  if (!foundWebhook) {
    result = "No web-hook was found.  Setting one up.";
    log(result);
    const webhook = await postWebhookCreate();
    await postWebhookUpdate(webhook);
  } else if (foundWebhook.status === "NEW_NOT_VERIFIED" || foundWebhook.status === "DISABLED_VERIFICATION_FAILED") {
    if (foundWebhook.disabledDetails) {
      result = "Found a disabled web webhook. Details %s will try to enable it. " + foundWebhook.disabledDetails;
      log(result);
    } else {
      result = "Updating webhook that wasn't verified";
    }
    postWebhookUpdate(foundWebhook)
  } else {
    log("Found a verified webhook.  Moving on: " + JSON.stringify(foundWebhook));
    log(result);
    result = {"Found Webhook: ": foundWebhook};
  }
  return result;
}

export const respondToWebhookChallenge = (challenge: string, res: Response) => {
  try {
    log("Getting the challenge from smart sheet: " + challenge + " and responding.");
    res.set(SMART_SHEETS_WEB_HOOK_RESPONSE_HEADER, challenge);
    return {smartsheetHookResponse: challenge};
    log("Responded to the challenge");
  } catch (e) {
    throw new Error("Error responding to challenge" + e.message);
  }
}
export const getRegisteredWebhook = async (callbackUrl: string) => {
  let foundWebhook;
  try {
    const options = {};

    const webhookList = await smartsheet.webhooks.listWebhooks(options);

    log("Listed webhook endpoint processing for %s", JSON.stringify(webhookList.data));
    // log("Listed webhook endpoint processing for %s", webhookList.data.length + " webhooks.");
    for (const webhook of webhookList.data) {
      if (webhook.callbackUrl === callbackUrl) {
        log("Found webhook for " + callbackUrl);
        foundWebhook = webhook;
        break;
      }
    }
  } catch (e) {
    throw new Error("Error determining if webhook is registered" + e.message);
  }
  return foundWebhook;
}
/**
 * Executes the multistep process of creating and enabling a web hook.
 * Once its enabled, smartsheets will verify if with another endpoint before
 * it can be used.
 * @param callbackUrl
 */
export const postWebhookCreate = async () => {
  try {
    log("Registering webhook %s", WEBHOOK_CALLBACK_URL);
    const body = {
      callbackUrl: WEBHOOK_CALLBACK_URL.trim(),
      events: ["*.*"],
      name: "POC Webhook",
      scope: "sheet",
      scopeObjectId: DEFAULT_SHEET_ID,
      version: 1
    };

    const createOptions = {
      body
    };
    const webhook = await smartsheet.webhooks.createWebhook(createOptions);
    return webhook;
  }
  catch (e) {
    throw new Error("Error getting smartSheets data " + e.message);
  }
}
