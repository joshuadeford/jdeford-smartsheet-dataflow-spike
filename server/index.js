
const serverless = require('serverless-http');
const express = require('express')
const app = express()

var AWSXRay = require('aws-xray-sdk');
app.use(AWSXRay.express.openSegment('MyApp'));

app.get('/', function (req, res) {
  res.send('Hello World!')
})

module.exports.handler = serverless(app);
app.use(AWSXRay.express.closeSegment());

