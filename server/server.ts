import debug from 'debug';
import express from "express";
const serverless = require('serverless-http');


import { addRow, getSheet, deleteRow } from './src/controllers/smart-sheets-provider'
import { WelcomeController } from './src/controllers/welcome.controller'

const app: express.Application = express();

const port: number = process.env.PORT as unknown as number || 3100;
const log = debug("SmartSheets");

const bodyParser = require('body-parser');
app.use(bodyParser.json())

// Mount the WelcomeController at the /welcome route
app.use('/welcome', WelcomeController);

// Serve the application at the given port
app.listen(port, () => {
  // Success callback
  // console.log(`Listening at http://localhost:${port}/`);
});

function setCORS(res: express.Response) {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Credentials', "true");
  res.set('Access-Control-Allow-Methods', "*");
}

app.get('/sheet/:sheetId?', async (req, res) => {
  setCORS(res);
  const sheetData: any = await getSheet(req, res);
  res.send( JSON.stringify(sheetData));
})
app.post('/sheet/', async (req, res) => {
  log(req.url + " posting");
  setCORS(res);
  const sheetData: any = await getSheet(req, res);
  res.send( JSON.stringify(sheetData));
})

app.patch('/sheet', async (req, res) => {
  log(req.url + " patching");
  setCORS(res);
  const sheetData: any = await addRow(req, res);
  res.send( JSON.stringify(sheetData));
});

app.delete('/sheet', async (req, res) => {
  log(req.url + " deleting");
  setCORS(res);
  const sheetData: any = await deleteRow(req, res);
  res.send( JSON.stringify(sheetData));
});

module.exports.handler = serverless(app);
